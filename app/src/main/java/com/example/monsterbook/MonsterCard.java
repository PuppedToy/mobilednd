package com.example.monsterbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MonsterCard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monster_card);
        Intent intent = getIntent();
        TextView tv = findViewById(R.id.textView);
        ImageView iv = findViewById(R.id.imageView);
        try {
            JSONObject monster = new JSONObject(intent.getStringExtra("monster"));
            tv.setText(monster.getString("name") + " (Level " + monster.getString("level") + ")");
            String image = monster.getString("image");
            if(!image.equals("B64")) {
                new DownloadImageTask(iv).execute(monster.getString("image"));
            } else {
                String base64Image = monster.getString("imageB64");
                byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                iv.setImageBitmap(bitmap);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
