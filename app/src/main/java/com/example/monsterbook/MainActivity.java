package com.example.monsterbook;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.database.SQLException;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private final static String CHANNEL_ID = "monster_channel_id";
    private JSONArray monstersList;
    private int index;
    private String[] monsterNameString;
    private ArrayAdapter<String> adapter;
    private final int CREATE_MONSTER_REQUEST_CODE = 1;

    private String getMonsterStringProperty(int index, String property) {
        try {
            return getMonster(index).getString(property);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject getMonster(int index) {
        try {
            return monstersList.getJSONObject(index);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class Console {
        public Console() {

        }

        public void log(String message) {
            Log.println(Log.INFO, "console.log", message);
        }
    }

    private Console console = new Console();

    private void alert(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private String getMonsterStringProperty(String property) {
        return getMonsterStringProperty(index, property);
    }

    private JSONObject getMonster() {
        return getMonster(index);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createNotificationChannel();
        setContentView(R.layout.activity_main);
        setupMonsters();
    }

    private void setupMonsters() {
        HttpGetRequest httpGet = new HttpGetRequest();
        String url = "http://home.viestregy.com:8081/api/monsters";
        JSONObject jsonResult;
        try {
            String result = httpGet.execute(url).get();
            index = 0;
            jsonResult = new JSONObject("{\"monsters\": " + result + "}");
            monstersList = new MonsterDBHelper(getApplicationContext()).getMonsterList();
            JSONArray monstersListFromAPI = jsonResult.getJSONArray("monsters");
            for(int i = 0; i < monstersListFromAPI.length(); i++) {
                monstersList.put(monstersListFromAPI.get(i));
            }

            /*Button nextButton = findViewById(R.id.button);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    index++;
                    TextView tv = findViewById(R.id.textView);
                    tv.setText(getMonsterStringProperty("name"));
                }
            });*/

            monsterNameString = new String[monstersList.length()];
            for(int i = 0; i < monstersList.length(); i++) {
                monsterNameString[i] = getMonsterStringProperty(i, "name");
            }
            ListView lv = findViewById(R.id.listView);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, monsterNameString);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView s = (TextView) view;
                    Intent intent  = new Intent(getApplicationContext(), MonsterCard.class);
                    intent.putExtra("monsterName", (String) s.getText());
                    intent.putExtra("monster", getMonster(position).toString());
                    startActivity(intent);
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Button add_monster_button = (Button) findViewById(R.id.add_monster_button);
        add_monster_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateMonster.class);
                startActivityForResult(intent, CREATE_MONSTER_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CREATE_MONSTER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                String name = data.getStringExtra("name");
                String level = data.getStringExtra("level");
                alert("Monster \"" + name + "\" of Level " + level + " created!");
                setupMonsters();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                alert("Monster canceled! :(");
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            alert("Some activity canceled!");
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
