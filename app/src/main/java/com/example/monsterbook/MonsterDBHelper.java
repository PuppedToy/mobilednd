package com.example.monsterbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MonsterDBHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "monsters";

    private final static String TABLE_MONSTER_NAME = "monster";
    private final static String TABLE_MONSTER_FULL_NAME = "`" + DATABASE_NAME + "`.`" + TABLE_MONSTER_NAME + "`";

    private final static int CURRENT_VERSION = 2;

    public MonsterDBHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, CURRENT_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_MONSTER_NAME + " (" +
                "  `id` INTEGER PRIMARY KEY AUTOINCREMENT," +
                "  `name` VARCHAR(300) NOT NULL," +
                "  `level` INTEGER NOT NULL DEFAULT 1," +
                "  `image` TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MONSTER_NAME);
        onCreate(db);
    }

    public JSONArray getMonsterList() throws SQLException, JSONException {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT name, level, image FROM " + TABLE_MONSTER_NAME, new String[] {});
        JSONArray result = new JSONArray();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            JSONObject object = new JSONObject();
            object.put("name", cursor.getString(0));
            object.put("level", cursor.getInt(1));
            object.put("image", "B64");
            object.put("imageB64", cursor.getString(2));
            result.put(object);
            cursor.moveToNext();
        }
        database.close();
        return result;
    }

    public long insertMonster(String name, int level, String image) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("level", level);
        values.put("image", image);
        long result = database.insert(TABLE_MONSTER_NAME, null, values);
        database.close();
        return result;
    }
}
