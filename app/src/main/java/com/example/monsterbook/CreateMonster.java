package com.example.monsterbook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;

public class CreateMonster extends AppCompatActivity {

    private final int TAKE_PICTURE_FROM_GALLERY_REQUEST = 1;
    private final int TAKE_PICTURE_FROM_CAMERA_REQUEST = 2;

    private class Console {
        public Console() {

        }

        public void log(String message) {
            Log.println(Log.INFO, "console.log", message);
        }
    }

    private Console console = new Console();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_monster);

        findViewById(R.id.upload_picture_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, TAKE_PICTURE_FROM_GALLERY_REQUEST);
            }
        });

        findViewById(R.id.take_photo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, TAKE_PICTURE_FROM_CAMERA_REQUEST);
                }
            }
        });

        Button save_button = (Button) findViewById(R.id.save_monster_button);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                EditText name_input = findViewById(R.id.monster_name_input);
                EditText level_input = findViewById(R.id.monster_level_input);
                ImageView monsterImage = (ImageView)findViewById(R.id.create_monster_image);
                BitmapDrawable drawable = (BitmapDrawable) monsterImage.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOS);
                String image = Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP);
                setResult(Activity.RESULT_OK, intent);
                String name = name_input.getText().toString();
                int level = Integer.parseInt(level_input.getText().toString());
                intent.putExtra("name", name);
                intent.putExtra("level", "" + level);
                intent.putExtra("image", image);
                try {
                    long id = new MonsterDBHelper(getApplicationContext()).insertMonster(name, level, image);
                    buildNotification(name, level, (int) id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE_FROM_CAMERA_REQUEST:
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ((ImageView)findViewById(R.id.create_monster_image)).setImageBitmap(imageBitmap);
                    break;
                case TAKE_PICTURE_FROM_GALLERY_REQUEST:
                    Uri selectedImage = data.getData();
                    ((ImageView)findViewById(R.id.create_monster_image)).setImageURI(selectedImage);
                    break;
            }
            if (requestCode == TAKE_PICTURE_FROM_GALLERY_REQUEST) {
                String name = data.getStringExtra("name");
                String level = data.getStringExtra("level");
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            console.log("Activity " + requestCode + " was canceled!");
        }
    }

    private void buildNotification(String name, int level, int notificationId) {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new
                NotificationCompat.Builder(this, "monster_channel_id")
                .setSmallIcon(R.mipmap.monster_icon)
                .setContentTitle("New monster added!")
                .setContentText("New monster \"" + name + "\" with level " + level + " has been added to the local storage")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, builder.build());

    }
}
